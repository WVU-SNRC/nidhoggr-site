package controllers

import play.api.DefaultGlobal
import play.api.mvc.{Action, Controller}

object Application extends Controller {
  def index = Action { implicit request =>
    Ok(views.html.index())
  }

  def about = Action { implicit request =>
    Ok(views.html.about())
  }

  def code = Action {implicit request =>
    Ok(views.html.code())
  }

  def pipeline = Action {
    implicit request =>
    Ok(views.html.pipeline())
  }

  def worker = Action{
    implicit request =>
    Ok (views.html.worker())
  }

  def serverStuff = Action{
    implicit request =>
    Ok(views.html.serverStuff())
  }

  def demo = Action{
    implicit request =>
    Ok(views.html.demo())
  }
}